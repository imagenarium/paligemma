import uvicorn
import os
import io
import torch
from PIL import Image
from transformers import PaliGemmaForConditionalGeneration, AutoProcessor
from fastapi import FastAPI, UploadFile


os.environ["HF_TOKEN"] = "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"
model_id = "google/paligemma-3b-mix-448"
app = FastAPI()


@app.on_event("startup")
def startup_event():
    app.model = PaliGemmaForConditionalGeneration.from_pretrained(model_id)
    app.processor = AutoProcessor.from_pretrained(model_id)


@app.post("/vision", name="VQA", tags=["AI Computer Vision"])
async def vision(file: UploadFile, query: str):
    print("Query:", query)
    image = Image.open(io.BytesIO(file.file.read())).convert('RGB')
    model_inputs = app.processor(text=query, images=image, return_tensors="pt")
    input_len = model_inputs["input_ids"].shape[-1]

    with torch.inference_mode():
        generation = app.model.generate(**model_inputs, max_new_tokens=256, do_sample=True, temperature=0.5, top_p=0.5, top_k=0)
        return app.processor.decode(generation[0][input_len:], skip_special_tokens=True)


if __name__ == "__main__":
    config = uvicorn.Config("main:app",
                            port=80,
                            host="0.0.0.0",
                            log_level="info",
                            workers=os.cpu_count())
    server = uvicorn.Server(config)
    server.run()