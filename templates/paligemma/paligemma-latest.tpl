<@requirement.NODE ref='paligemma' primary='paligemma-${namespace}' single='false' />
<@requirement.PARAM name='API_PORT' required='false' type='port' scope='global' />

<#if tag??>
  <@img.TASK 'paligemma-${namespace}' 'imagenarium/paligemma:${tag}'>
    <@img.NODE_REF 'paligemma' />
    <@img.VOLUME '/root/.cache' />
    <@img.PORT PARAMS.API_PORT '80' />
    <@img.CHECK_PORT '80' />
  </@img.TASK>
<#else>
  <@img.ALERT 'Укажите тег для развертывания!' />
</#if>
