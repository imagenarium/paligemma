#!/bin/bash
source ./cilib.sh
source ./env.sh &> /dev/null || true

init ${IMG_HOST}:5555 ${IMG_USER} ${IMG_PASSWORD}

repo paligemma https://gitlab.com/imagenarium/paligemma

labels ${NODES[0]} paligemma-${NAMESPACE}

from paligemma ${TAG}
fresh false
param API_PORT 8000
deploy_stack ${NAMESPACE} paligemma latest
